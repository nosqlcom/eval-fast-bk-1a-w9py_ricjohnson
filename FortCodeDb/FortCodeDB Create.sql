USE [master]
GO
/****** Object:  Database [FortCodeDb]    Script Date: 5/17/2021 2:02:37 PM ******/
CREATE DATABASE [FortCodeDb]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'FortCodeDb', FILENAME = N'/var/opt/mssql/data/FortCodeDb.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'FortCodeDb_log', FILENAME = N'/var/opt/mssql/data/FortCodeDb_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [FortCodeDb] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [FortCodeDb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [FortCodeDb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [FortCodeDb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [FortCodeDb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [FortCodeDb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [FortCodeDb] SET ARITHABORT OFF 
GO
ALTER DATABASE [FortCodeDb] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [FortCodeDb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [FortCodeDb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [FortCodeDb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [FortCodeDb] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [FortCodeDb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [FortCodeDb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [FortCodeDb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [FortCodeDb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [FortCodeDb] SET  DISABLE_BROKER 
GO
ALTER DATABASE [FortCodeDb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [FortCodeDb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [FortCodeDb] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [FortCodeDb] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [FortCodeDb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [FortCodeDb] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [FortCodeDb] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [FortCodeDb] SET RECOVERY FULL 
GO
ALTER DATABASE [FortCodeDb] SET  MULTI_USER 
GO
ALTER DATABASE [FortCodeDb] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [FortCodeDb] SET DB_CHAINING OFF 
GO
ALTER DATABASE [FortCodeDb] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [FortCodeDb] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [FortCodeDb] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'FortCodeDb', N'ON'
GO
ALTER DATABASE [FortCodeDb] SET QUERY_STORE = OFF
GO
USE [FortCodeDb]
GO
/****** Object:  Table [dbo].[AccountFavoriteCitiesSecretBars]    Script Date: 5/17/2021 2:02:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountFavoriteCitiesSecretBars](
	[AccountID] [int] NOT NULL,
	[CityID] [int] NOT NULL,
	[BarID] [int] NULL,
	[FavoriteDrink] [int] NULL,
	[FakeID] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_AccountFavoriteCitiesSecretBars] PRIMARY KEY CLUSTERED 
(
	[FakeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Accounts]    Script Date: 5/17/2021 2:02:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Accounts](
	[AccountID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](50) NOT NULL,
	[AuthToken] [varchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[IsAdmin] [bit] NOT NULL,
 CONSTRAINT [PK_Accounts] PRIMARY KEY CLUSTERED 
(
	[AccountID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Bar]    Script Date: 5/17/2021 2:02:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bar](
	[BarID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[CityID] [int] NOT NULL,
 CONSTRAINT [PK_Bar] PRIMARY KEY CLUSTERED 
(
	[BarID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[City]    Script Date: 5/17/2021 2:02:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[City](
	[CityID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[CountryID] [int] NOT NULL,
 CONSTRAINT [PK_City] PRIMARY KEY CLUSTERED 
(
	[CityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Country]    Script Date: 5/17/2021 2:02:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Country](
	[CountryID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED 
(
	[CountryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Drink]    Script Date: 5/17/2021 2:02:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Drink](
	[DrinkID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Drink] PRIMARY KEY CLUSTERED 
(
	[DrinkID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Index [IXU_AccountFavoriteCitiesSecretBars]    Script Date: 5/17/2021 2:02:37 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IXU_AccountFavoriteCitiesSecretBars] ON [dbo].[AccountFavoriteCitiesSecretBars]
(
	[AccountID] ASC,
	[BarID] ASC,
	[CityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IXU_Accounts_Email]    Script Date: 5/17/2021 2:02:37 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IXU_Accounts_Email] ON [dbo].[Accounts]
(
	[Email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IXU_Bar_Name]    Script Date: 5/17/2021 2:02:37 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IXU_Bar_Name] ON [dbo].[Bar]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IXU_City_Name_Country]    Script Date: 5/17/2021 2:02:37 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IXU_City_Name_Country] ON [dbo].[City]
(
	[Name] ASC,
	[CountryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IXU_Country_Name]    Script Date: 5/17/2021 2:02:37 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IXU_Country_Name] ON [dbo].[Country]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IXU_Drink_Name]    Script Date: 5/17/2021 2:02:37 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IXU_Drink_Name] ON [dbo].[Drink]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Accounts] ADD  CONSTRAINT [DF_Accounts_IsAdmin]  DEFAULT ((0)) FOR [IsAdmin]
GO
ALTER TABLE [dbo].[AccountFavoriteCitiesSecretBars]  WITH CHECK ADD  CONSTRAINT [FK_AccountFavoriteCities_Accounts] FOREIGN KEY([AccountID])
REFERENCES [dbo].[Accounts] ([AccountID])
GO
ALTER TABLE [dbo].[AccountFavoriteCitiesSecretBars] CHECK CONSTRAINT [FK_AccountFavoriteCities_Accounts]
GO
ALTER TABLE [dbo].[AccountFavoriteCitiesSecretBars]  WITH CHECK ADD  CONSTRAINT [FK_AccountFavoriteCities_City] FOREIGN KEY([CityID])
REFERENCES [dbo].[City] ([CityID])
GO
ALTER TABLE [dbo].[AccountFavoriteCitiesSecretBars] CHECK CONSTRAINT [FK_AccountFavoriteCities_City]
GO
ALTER TABLE [dbo].[AccountFavoriteCitiesSecretBars]  WITH CHECK ADD  CONSTRAINT [FK_AccountFavoriteCitiesSecretBars_Bar] FOREIGN KEY([BarID])
REFERENCES [dbo].[Bar] ([BarID])
GO
ALTER TABLE [dbo].[AccountFavoriteCitiesSecretBars] CHECK CONSTRAINT [FK_AccountFavoriteCitiesSecretBars_Bar]
GO
ALTER TABLE [dbo].[AccountFavoriteCitiesSecretBars]  WITH CHECK ADD  CONSTRAINT [FK_AccountFavoriteCitiesSecretBars_Drink] FOREIGN KEY([FavoriteDrink])
REFERENCES [dbo].[Drink] ([DrinkID])
GO
ALTER TABLE [dbo].[AccountFavoriteCitiesSecretBars] CHECK CONSTRAINT [FK_AccountFavoriteCitiesSecretBars_Drink]
GO
ALTER TABLE [dbo].[Bar]  WITH CHECK ADD  CONSTRAINT [FK_Bar_City] FOREIGN KEY([CityID])
REFERENCES [dbo].[City] ([CityID])
GO
ALTER TABLE [dbo].[Bar] CHECK CONSTRAINT [FK_Bar_City]
GO
ALTER TABLE [dbo].[City]  WITH CHECK ADD  CONSTRAINT [FK_City_Country] FOREIGN KEY([CountryID])
REFERENCES [dbo].[Country] ([CountryID])
GO
ALTER TABLE [dbo].[City] CHECK CONSTRAINT [FK_City_Country]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Account Email' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Accounts', @level2type=N'INDEX',@level2name=N'IXU_Accounts_Email'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Un‪ique Bar Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Bar', @level2type=N'INDEX',@level2name=N'IXU_Bar_Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique City Name by Country' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'City', @level2type=N'INDEX',@level2name=N'IXU_City_Name_Country'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique Country Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Country', @level2type=N'INDEX',@level2name=N'IXU_Country_Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Un‪ique Drink Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Drink', @level2type=N'INDEX',@level2name=N'IXU_Drink_Name'
GO
USE [master]
GO
ALTER DATABASE [FortCodeDb] SET  READ_WRITE 
GO
