﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

namespace FortData
{
    public partial class Accounts
    {
        public Accounts()
        {
            AccountFavoriteCitiesSecretBars = new HashSet<AccountFavoriteCitiesSecretBars>();
        }

        public int AccountId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string AuthToken { get; set; }
        public string Password { get; set; }
        public bool IsAdmin { get; set; }

        public virtual ICollection<AccountFavoriteCitiesSecretBars> AccountFavoriteCitiesSecretBars { get; set; }
    }
}