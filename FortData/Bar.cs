﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

namespace FortData
{
    public partial class Bar
    {
        public Bar()
        {
            AccountFavoriteCitiesSecretBars = new HashSet<AccountFavoriteCitiesSecretBars>();
        }

        public int BarId { get; set; }
        public string Name { get; set; }
        public int CityId { get; set; }

        public virtual City City { get; set; }
        public virtual ICollection<AccountFavoriteCitiesSecretBars> AccountFavoriteCitiesSecretBars { get; set; }
    }
}