﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

namespace FortData
{
    public partial class City
    {
        public City()
        {
            AccountFavoriteCitiesSecretBars = new HashSet<AccountFavoriteCitiesSecretBars>();
            Bar = new HashSet<Bar>();
        }

        public int CityId { get; set; }
        public string Name { get; set; }
        public int CountryId { get; set; }

        public virtual Country Country { get; set; }
        public virtual ICollection<AccountFavoriteCitiesSecretBars> AccountFavoriteCitiesSecretBars { get; set; }
        public virtual ICollection<Bar> Bar { get; set; }
    }
}