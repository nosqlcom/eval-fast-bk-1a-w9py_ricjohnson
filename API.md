Base URL: http://localhost:49632/api/accounts

1) /authenticate
	Description: Given an email and a password, verify and pass back an auth token with Name
	POST (JSON):
		{
			"Email": "Frank@Frank.com",
			"Password": "password1"
		}	
	RESPONSE:
		{
			"authToken": "637568216880991866",
			"name": "Frank",
			"isAdmin": false
		}
	NOTES:
		a) Should hash, salt, and use a nonce for auth
		b) Delete / Set account as Admin not needed
		c) // TODO: Remove isAdmin flag from result
		d) Auth token is just the current time ticks.  Upon successful auth, it is stored back in the DB for verification of other calls

2) /Create
	Description: Given an authenticated user token of an Admin, and a new Name, Email, and Password of a new user, create the new user and pass back an user ID	POST (JSON):
	POST (JSON)
		{
			"AuthToken": "637568196460810207",
			"Name": "TestName03",
			"Email": "TestEmail03@free.tv",
			"Password": "password1"
		}
	RESPONSE:
		{
			"accountID": 26
		}
	NOTES: 
		a) User should be an Admin to create new users

3) /FavoriteCities
	Description: Given an authenticated user token, return a list of favorit cities
	POST (JSON)
		{
			"AuthToken": "637568196460810207",
		}
	RESPONSE:
		{
			"accountID": 6,
			"favoriteCities": [
				{
					"cityId": 7,
					"name": "Freeport",
					"countryId": 2
				},
				{
					"cityId": 14,
					"name": "Guayaquil",
					"countryId": 5
				}
			]
		}
	NOTES:
		a) This reponse is for eve
		b) TODO: Return Country Name

3) /AddFavoriteCity
	Description: Given an authenticated user token, CityName, and CountryName - add City to favorite list for user.  Also Create country and City if they do not exist.  Returns Ids of City and Country
	POST (JSON):
		{
			"AuthToken": "637568197191461796",
			"CityName": "TestCountry04-TestCity04",
			"CountryName": "TestCountry04"
		}
	RESPONSE:
		{
			"cityId": 20,
			"countryId": 10
		}
	NOTES:
		a) Can the same city name exist in multiple countries?

4) /RemoveFavoriteCity
	Description: Given an authenticated user token and a CityID - remove City from favorites list for user.	Return numRemoved which is the count of Secret bars for that user + city
	POST (JSON):
		{
			"AuthToken": "637568197191461796",
			"CityName": "TestCountry04-TestCity04",
			"CountryName": "TestCountry04"
		}
	RESPONSE:
		{
			"cityId": 20,
			"numRemoved": 1
		}
	NOTES:
		a) Does NOT Delete the City or country from list of avaialbe Cities
		b) CityID used instead of CityName as there may be multiple CityNames in different countries

5) /SecretBars 
	Description: Given an authenticated user token and a CityID - return a list of secret bars for that user + city
	POST (JSON):
		{
			"AuthToken": "637568216880991866",
			"CityId": 5
		}
	RESPONSE:
		{
			"accountID": 8,
			"secretBars": [
				{
					"barId": 8,
					"barName": "Nassau Bar 1"
				},
				{
					"barId": 10,
					"barName": "Nassau Bar 3"
				}
			]
		}
	NOTES:
		a) Also returns accountID.  // TODO - do we need this?  Should I add to other APIs?	

6) /AddSecretBar
	Description: Given an authenticated user token, CityID, BarName, and FavoriteDrink, add Bar to secrets bars for user.  Also creates Bar and Drink if needed. Return BarID and DrinkId
	POST (JSON):
		{
			"AuthToken": "637568477657104823",
			"CityId": 7,
			"BarName": "NoNameBar03",
			"FavoriteDrink": "TestDrink5"
		}	
	RESPONSE:
		{
			"barId": 1018,
			"drinkId": 19
		}		
	NOTES:
		a) FavoriteDrink may be NULL
		b) Drink names are unique

6) /SharedBars
	Description: Given an authenticated user token and CityID, get a list of Secret bars that are shared by other accounts for that user
	POST (JSON):
		{
			"AuthToken": "637568501551374065",
			"CityId": 4
		}
	RESPONSE:
		{
			"accountID": 13,
			"sharedBars": [
				{
					"bardId": 7,
					"barName": "Perth Bar 1",
				} 
			]
		}
	NOTES:
		a)  Returns Account Id - is that needed?

6) /RemoveSecretBar
	Description: Given an authenticated user token and BarID, remove that ber from the list  Secret bars for that user
	POST (JSON):
		{
			"AuthToken": "637568216880991866",
			"BarId": 10
		}
	RESPONSE:
		{
			"barId": 10
		}
	NOTES:
		a) Does not delete Bar or Drink
