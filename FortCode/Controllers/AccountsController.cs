﻿using FortCode.Models;
using FortData;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        private readonly FortcodedbContext _context;

        public AccountsController(FortcodedbContext context)
        {
            _context = context;
        }

        [HttpPost("[action]")]
        public async Task<ActionResult<AuthenicateResponse>> Authenticate(AuthenticateRequest authenticateRequest)
        {
            AuthenicateResponse authenicateResponse = new AuthenicateResponse();

            var account = _context.Accounts.FirstOrDefault(act => act.Email.ToLower() == authenticateRequest.Email.ToLower());
            if (account == null)
            {
                return NotFound();
            }

            if (account.Password != authenticateRequest.Password)
            {
                return BadRequest("Invalid password");
            }

            account.AuthToken = DateTime.Now.Ticks.ToString();
            await _context.SaveChangesAsync();

            authenicateResponse.Name = account.Name;
            authenicateResponse.AuthToken = account.AuthToken;
            authenicateResponse.IsAdmin = account.IsAdmin;  // TODO - may not need to return this, only on Create Account

            return authenicateResponse;
        }

        [HttpPost("[action]")]
        public async Task<ActionResult<IEnumerable<Accounts>>> GetAll(GetAccountsRequest getAccountsRequest)
        {
            var account = _context.Accounts.FirstOrDefault(act => act.AuthToken == getAccountsRequest.AuthToken);
            if (account == null)
            {
                return NotFound();
            }

            return await _context.Accounts.ToListAsync();
        }

        [HttpPost("[action]")]
        public async Task<ActionResult<CreateAccountResponse>> Create(CreateAccountRequest createAccountRequest)
        {
            CreateAccountResponse createAccountResponse = new CreateAccountResponse();

            var account = _context.Accounts.FirstOrDefault(act => act.AuthToken == createAccountRequest.AuthToken);
            if (account == null)
            {
                return NotFound();
            }

            if (!account.IsAdmin)
            {
                return BadRequest("Need admin to Create Account");
            }

            try
            {
                Accounts newAccount = new Accounts();
                newAccount.Name = createAccountRequest.Name;
                newAccount.Password = createAccountRequest.Password;
                newAccount.Email = createAccountRequest.Email;
                //newAccount.IsAdmin = false;
                newAccount.AuthToken = "";

                _context.Accounts.Add(newAccount);
                await _context.SaveChangesAsync();

                createAccountResponse.AccountID = newAccount.AccountId;
            }
            catch (Exception ex)
            {
                return BadRequest("Could not create account");
            }

            return createAccountResponse;
        }

        [HttpPost("[action]")]
        public async Task<ActionResult<FavoriteCityResponse>> FavoriteCities(GetAccountsRequest getAccountsRequest)
        {
            var account = _context.Accounts.FirstOrDefault(act => act.AuthToken == getAccountsRequest.AuthToken);
            if (account == null)
            {
                return NotFound();
            }

            var AccountFavoriteCitiesSecretBarsList = _context.AccountFavoriteCitiesSecretBars.Where(act => act.AccountId == account.AccountId).ToList();

            if (AccountFavoriteCitiesSecretBarsList == null || AccountFavoriteCitiesSecretBarsList.Count == 0)
            {
                return NotFound("No favorite cites");
            }

            var cityIDs = AccountFavoriteCitiesSecretBarsList.Select(act => act.CityId).ToList();
            var cities = await _context.City.Where(city => cityIDs.Contains(city.CityId)).ToListAsync();

            FavoriteCityResponse favoriteCityResponse = new FavoriteCityResponse();
            favoriteCityResponse.AccountID = account.AccountId;
            foreach (City city in cities) {
                CityResponse cityResponse = new CityResponse();
                cityResponse.CityId = city.CityId;
                cityResponse.CountryId = city.CountryId;
                cityResponse.Name = city.Name;
                favoriteCityResponse.FavoriteCities.Add(cityResponse);
            }

            return favoriteCityResponse;
        }

        [HttpPost("[action]")]
        public async Task<ActionResult<GetSecretBarsResponse>> SecretBars(GetSecretBarsRequest getSecretBarsRequest)
        {
            var account = _context.Accounts.FirstOrDefault(act => act.AuthToken == getSecretBarsRequest.AuthToken);
            if (account == null)
            {
                return NotFound();
            }

            var accountFavoriteCitiesSecretBarsList = _context.AccountFavoriteCitiesSecretBars.Where(act => act.AccountId == account.AccountId).ToList();

            if (accountFavoriteCitiesSecretBarsList == null || accountFavoriteCitiesSecretBarsList.Count == 0)
            {
                return NotFound("No favorite cites");
            }

            var accountFavoriteCitiesSecretBars = accountFavoriteCitiesSecretBarsList.Where(fav => fav.CityId == getSecretBarsRequest.CityID && fav.BarId != null);

            GetSecretBarsResponse getSecretBarsResponse = new GetSecretBarsResponse();
            getSecretBarsResponse.AccountID = account.AccountId;
            foreach (AccountFavoriteCitiesSecretBars favSecretBars in accountFavoriteCitiesSecretBars)
            {
                SecretBarsResponse secretBarsResponse = new SecretBarsResponse();

                secretBarsResponse.BarId = favSecretBars.BarId;

                var barNameFind = _context.Bar.Where(ba => ba.BarId == favSecretBars.BarId).FirstOrDefault();
                if (barNameFind != null)
                {
                    secretBarsResponse.BarName = barNameFind.Name;
                }

                getSecretBarsResponse.SecretBars.Add(secretBarsResponse);
            }

            return getSecretBarsResponse;
        }

        [HttpPost("[action]")]
        public async Task<ActionResult<SharedBarsResponse>> SharedBars(SharedBarsRequest sharedBarsRequest)
        {
            var account = _context.Accounts.FirstOrDefault(act => act.AuthToken == sharedBarsRequest.AuthToken);
            if (account == null)
            {
                return NotFound();
            }

            var accountFavoriteCitiesSecretBarsList = _context.AccountFavoriteCitiesSecretBars.Where(act => act.AccountId == account.AccountId).ToList();

            if (accountFavoriteCitiesSecretBarsList == null || accountFavoriteCitiesSecretBarsList.Count == 0)
            {
                return NotFound("No favorite cites");
            }

            var listOfMyBars = accountFavoriteCitiesSecretBarsList.Where(fav => fav.BarId != null).ToList();
            if (listOfMyBars == null || listOfMyBars.Count == 0)
            {
                return NotFound("No secret bars");
            }


            SharedBarsResponse sharedBarsResponse = new SharedBarsResponse();
            sharedBarsResponse.AccountID = account.AccountId;
            BarsResponse barsResponse;
            foreach (var favBar in listOfMyBars)
            {
                var sameBarOtherAccount = _context.AccountFavoriteCitiesSecretBars.Where(act => act.BarId == favBar.BarId && act.AccountId != account.AccountId).ToList();  
                foreach(var sameBarOther in sameBarOtherAccount)
                {
                    barsResponse = sharedBarsResponse.SharedBars.Where(sb => sb.BardId == favBar.BarId).FirstOrDefault();

                    if (barsResponse == null)
                    {
                        barsResponse = new BarsResponse();

                        barsResponse.BardId = favBar.BarId ?? 0;
                        var findBarById = _context.Bar.Where(ba => ba.BarId == favBar.BarId).FirstOrDefault();
                        if(findBarById != null)
                        {
                            barsResponse.BarName = findBarById.Name;
                        }
                        sharedBarsResponse.SharedBars.Add(barsResponse);
                    }
                    barsResponse.NumOtherAccounts++;
                }
            }

            return sharedBarsResponse;
        }

        [HttpPost("[action]")]
        public async Task<ActionResult<AddSecretBarResponse>> AddSecretBar(AddSecretBarRequest addSecretBarRequest)
        {
            AddSecretBarResponse addSecretBarResponse = new AddSecretBarResponse();

            var account = _context.Accounts.FirstOrDefault(act => act.AuthToken == addSecretBarRequest.AuthToken);
            if (account == null)
            {
                return NotFound();
            }
            int accountId = account.AccountId;
                
            List<AccountFavoriteCitiesSecretBars> accountFavoriteCitiesSecretBarsList = _context.AccountFavoriteCitiesSecretBars.Where(act => act.AccountId == accountId).ToList();

            if (accountFavoriteCitiesSecretBarsList == null || accountFavoriteCitiesSecretBarsList.Count == 0)
            {
                return NotFound("No favorite cites");
            }

            var favoriteCitySecretBarsList = accountFavoriteCitiesSecretBarsList.Where(actFav => actFav.AccountId == accountId && actFav.CityId == addSecretBarRequest.CityId).ToList();
            if(favoriteCitySecretBarsList == null || favoriteCitySecretBarsList.Count == 0)
            {
                return NotFound("City is not a favorite");
            }

            var newBar = _context.Bar.Where(b => b.Name == addSecretBarRequest.BarName).FirstOrDefault();
            if(newBar == null)
            {
                try
                {
                    newBar = new Bar();
                    newBar.CityId = addSecretBarRequest.CityId;
                    newBar.Name = addSecretBarRequest.BarName;

                    _context.Bar.Add(newBar);
                    _context.Entry(newBar).State = EntityState.Added;

                    await _context.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    return BadRequest("Could not create bar");
                }
            }
            addSecretBarResponse.BarId = newBar.BarId;

            addSecretBarResponse.DrinkId = null;
            if (!string.IsNullOrEmpty(addSecretBarRequest.FavoriteDrink))
            {
                var newDrink = _context.Drink.Where(dr => dr.Name == addSecretBarRequest.FavoriteDrink).FirstOrDefault();
                if (newDrink == null)
                {
                    try
                    {
                        newDrink = new Drink();
                        newDrink.Name = addSecretBarRequest.FavoriteDrink;

                        _context.Drink.Add(newDrink);
                        _context.Entry(newDrink).State = EntityState.Added;
                        await _context.SaveChangesAsync();
                    }
                    catch (Exception ex)
                    {
                        return BadRequest("Could not create Drink");
                    }
                }
                addSecretBarResponse.DrinkId = newDrink.DrinkId;
            }

            var favoriteCitySecretBar = _context.AccountFavoriteCitiesSecretBars.Where(fav => fav.AccountId == accountId && fav.CityId == addSecretBarRequest.CityId).FirstOrDefault();
            if (favoriteCitySecretBar == null)
            {
                favoriteCitySecretBar = new AccountFavoriteCitiesSecretBars();

                favoriteCitySecretBar.AccountId = accountId;
                favoriteCitySecretBar.CityId = addSecretBarRequest.CityId;
                favoriteCitySecretBar.BarId = addSecretBarResponse.BarId;
                _context.Entry(favoriteCitySecretBar).State = EntityState.Added;
                await _context.SaveChangesAsync();
            }

            favoriteCitySecretBar.BarId = newBar.BarId;
            favoriteCitySecretBar.FavoriteDrink = addSecretBarResponse.DrinkId;
            
            // TODO: add Primary key to track
            _context.Entry(favoriteCitySecretBar).State = EntityState.Modified;

            await _context.SaveChangesAsync();

            return addSecretBarResponse;
        }

        [HttpPost("[action]")]
        public async Task<ActionResult<RemoveFavoriteCityResponse>> RemoveFavoriteCity(RemoveFavoriteCityRequest removeFavoriteCityRequest)
        {
            RemoveFavoriteCityResponse removeFavoriteCityResponse = new RemoveFavoriteCityResponse();

            var account = _context.Accounts.FirstOrDefault(act => act.AuthToken == removeFavoriteCityRequest.AuthToken);
            if (account == null)
            {
                return NotFound();
            }
            int accountId = account.AccountId;

            List<AccountFavoriteCitiesSecretBars> accountFavoriteCitiesSecretBarsList = _context.AccountFavoriteCitiesSecretBars.Where(act => act.AccountId == accountId && act.CityId == removeFavoriteCityRequest.CityId).ToList();

            if (accountFavoriteCitiesSecretBarsList == null || accountFavoriteCitiesSecretBarsList.Count == 0)
            {
                return NotFound("Favorite city not found");
            }

            foreach(var fav in accountFavoriteCitiesSecretBarsList)
            {
                removeFavoriteCityResponse.CityId = fav.CityId;
                removeFavoriteCityResponse.NumRemoved++;
                _context.AccountFavoriteCitiesSecretBars.Remove(fav);
            }
            await _context.SaveChangesAsync();

            return removeFavoriteCityResponse;
        }

        [HttpPost("[action]")]
        public async Task<ActionResult<RemoveSecretBarResponse>> RemoveSecretBar(RemoveSecretBarRequest removeSecretBarRequest)
        {
            RemoveSecretBarResponse removeSecretBarResponse = new RemoveSecretBarResponse();

            var account = _context.Accounts.FirstOrDefault(act => act.AuthToken == removeSecretBarRequest.AuthToken);
            if (account == null)
            {
                return NotFound();
            }
            int accountId = account.AccountId;

            List<AccountFavoriteCitiesSecretBars> accountFavoriteCitiesSecretBarsList = _context.AccountFavoriteCitiesSecretBars.Where(act => act.AccountId == accountId).ToList();

            if (accountFavoriteCitiesSecretBarsList == null || accountFavoriteCitiesSecretBarsList.Count == 0)
            {
                return NotFound("Favorite city not found");
            }

            var accountFavoriteCitiesSecretBar = accountFavoriteCitiesSecretBarsList.Where(fav => fav.BarId == removeSecretBarRequest.BarId).FirstOrDefault();

            if (accountFavoriteCitiesSecretBar == null)
            {
                return NotFound("Bar is not part of secret list for this user");
            }

            _context.AccountFavoriteCitiesSecretBars.Remove(accountFavoriteCitiesSecretBar);            
            await _context.SaveChangesAsync();

            removeSecretBarResponse.BarId = removeSecretBarRequest.BarId;
            return removeSecretBarResponse;
        }

        [HttpPost("[action]")]
        public async Task<ActionResult<AddFavoriteCityResponse>> AddFavoriteCity(AddFavoriteCityRequest addFavoriteCityRequest)
        {
            AddFavoriteCityResponse addFavoriteCityResponse = new AddFavoriteCityResponse();

            var account = _context.Accounts.FirstOrDefault(act => act.AuthToken == addFavoriteCityRequest.AuthToken);
            if (account == null)
            {
                return NotFound();
            }
            int accountId = account.AccountId;

            var newCountry = _context.Country.Where(con => con.Name == addFavoriteCityRequest.CountryName).FirstOrDefault();
            if (newCountry == null)
            {
                try
                {
                    newCountry = new Country();
                    newCountry.Name = addFavoriteCityRequest.CountryName;

                    _context.Country.Add(newCountry);
                    _context.Entry(newCountry).State = EntityState.Added;

                    await _context.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    return BadRequest("Could not create Country");
                }
            }
            addFavoriteCityResponse.CountryId = newCountry.CountryId;

            var newCity = _context.City.Where(cit => cit.CountryId == newCountry.CountryId && cit.Name == addFavoriteCityRequest.CityName).FirstOrDefault();
            if (newCity == null)
            {
                try
                {
                    newCity = new City();
                    newCity.CountryId = newCountry.CountryId;
                    newCity.Name = addFavoriteCityRequest.CityName;

                    _context.City.Add(newCity);
                    _context.Entry(newCity).State = EntityState.Added;
                    await _context.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    return BadRequest("Could not create City");
                }
            }
            addFavoriteCityResponse.CityId = newCity.CityId;

            List<AccountFavoriteCitiesSecretBars> accountFavoriteCitiesSecretBarsList = _context.AccountFavoriteCitiesSecretBars.Where(act => act.AccountId == accountId && act.CityId == newCity.CityId).ToList();

            if (accountFavoriteCitiesSecretBarsList == null || accountFavoriteCitiesSecretBarsList.Count == 0)
            {
                var favoriteCitySecretBar = new AccountFavoriteCitiesSecretBars();

                favoriteCitySecretBar.AccountId = accountId;
                favoriteCitySecretBar.CityId = newCity.CityId;
                favoriteCitySecretBar.BarId = null;
                favoriteCitySecretBar.FavoriteDrink = null;
                _context.Entry(favoriteCitySecretBar).State = EntityState.Added;
                await _context.SaveChangesAsync();
            }

            return addFavoriteCityResponse;
        }

    }
}
