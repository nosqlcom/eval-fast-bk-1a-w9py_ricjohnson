using FortData;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace FortCode
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
    
        public virtual void ConfigureServices(IServiceCollection services)
        {
            services
                .AddMvc();

            services
                .AddControllers();

            // TODO: Use options for Connection string
            services.AddDbContext<FortcodedbContext>();
        }

        public virtual void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // TODO: app.UseHttpsRedirection();

            app
                .UseRouting()
                .UseEndpoints(endPoints => { endPoints.MapControllers(); })
                .UseFileServer()
            ;

            // TODO: app.UseAuthorization();

        }
    }
}
