﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode.Models
{
    public class AuthBaseRequest
    {
        public string AuthToken { get; set; }
    }
}
