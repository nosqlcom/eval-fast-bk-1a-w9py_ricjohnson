﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode.Models
{
    public class AddSecretBarRequest: AuthBaseRequest
    {
        public int CityId { get; set; }

        public string BarName { get; set; }
        public string? FavoriteDrink { get; set; } 
    }
}
