﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode.Models
{
    public class RemoveFavoriteCityRequest : AuthBaseRequest
    {
        public int CityId { get; set; }
    }
}
