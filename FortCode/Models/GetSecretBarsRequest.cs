﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode.Models
{
    public class GetSecretBarsRequest : AuthBaseRequest
    {
        public int CityID { get; set; }
    }
}
