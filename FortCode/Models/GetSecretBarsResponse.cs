﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode.Models
{
    public class GetSecretBarsResponse
    {
        public int AccountID { get; set; }

        public List<SecretBarsResponse> SecretBars { get; set; }

        public GetSecretBarsResponse()
        {
            SecretBars = new List<SecretBarsResponse>();
        }
    }
}
