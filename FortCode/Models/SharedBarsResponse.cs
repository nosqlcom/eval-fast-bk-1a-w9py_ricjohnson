﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode.Models
{
    public class SharedBarsResponse
    {
        public int AccountID { get; set; }

        public List<BarsResponse> SharedBars { get; set; }

        public SharedBarsResponse()
        {
            SharedBars = new List<BarsResponse>();
        }
    }
}
