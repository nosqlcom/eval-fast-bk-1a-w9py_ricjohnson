﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode.Models
{
    public class SecretBarsResponse
    {
        public int? BarId { get; set; }
        public string BarName { get; set; }

        public SecretBarsResponse()
        {
            BarName = "";
        }

        // TODO
        // public int? FavoriteDrinkId { get; set; }

        // TODO
        // public string FavoriteDrinkName { get; set; }
    }
}
