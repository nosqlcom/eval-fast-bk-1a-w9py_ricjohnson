﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode.Models
{
    public class RemoveSecretBarRequest : AuthBaseRequest
    {
        public int BarId { get; set; }
    }
}
