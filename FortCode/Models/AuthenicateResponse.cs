﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode.Models
{
    public class AuthenicateResponse
    {
        public string AuthToken { get; set; }
        public string Name { get; set; }

        public bool IsAdmin { get; set; }
    }
}
