﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode.Models
{
    public class AddSecretBarResponse
    {
        public int BarId { get; set; }
        public int? DrinkId { get; set; }
    }
}
