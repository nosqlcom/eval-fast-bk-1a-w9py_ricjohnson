﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode.Models
{
    public class CityResponse
    {
        public int CityId { get; set; }
        public string Name { get; set; }
        public int CountryId { get; set; }
    }
}
