﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode.Models
{
    public class FavoriteCityResponse
    {
        public int AccountID { get; set; }

        public List<CityResponse> FavoriteCities { get; set; }

        public FavoriteCityResponse()
        {
            FavoriteCities = new List<CityResponse>();
        }
    }
}
