﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode.Models
{
    public class BarsResponse
    {
        public int BardId { get; set; }
        public string BarName { get; set; }
        public int NumOtherAccounts { get; set; }
    }
}
