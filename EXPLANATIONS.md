Information on calling API is in API.md

Database design in FortCodeDb folder.  
FortCodeDB Create.sql creates the whole schema
TestData files have the inital test data. (This was not updated after testing all endpoints)

FortData Directory is the ORM (Entity Framework)
TODO: move connection string to config and encrypt

Issues:
1) I could not get Docker to work for the longest time.  My personal PC did not have the capabilities, so I created a VM in the cloud.  But to have a NESTED container is limited to specific operating systems and machine types that allow Hyper-V.
2) Once I got the Docker up and running, I could not connect to the DB server.  The password was not in any of the solution files - it was in the docker config. I was confused if the password used the <> characters. Also it uses a non-standard PORT (because it was on Docker), but I kept on using colon for the port instead of a comma.
3) Where is the database? I lost some time looking for the DB instance.  I assume now that there is no DB and I should create it.  No problem - I can do that and enforce 4th normal with foreign keys and even unique constraints, but asking this to do this is extraordinary. FORT Robotics is an extraordinary company, so I am willing to do the work.
4) I needed to create FakeID in AccountFavoriteCitiesSecretBars to be able save changes with NULL
5) Setting up Service controller in current code tookk time to hook up properly

1) DB design change - Account Favorite cities plus Secerts bars (AND Drinks) - avoids circular relationship
	Testing data normaliztion
	a) Account with NO favorite cities 
		Alice
	b) Account with Favorite city but no secret bars
		Bob in Aarhus
	c) 1 account has favorite city and secret bar but no favorite drink
		Charlie in Brisbane - Bribanke Bar 1
	d) 1 acoount has favorite city and favorie bar with favorite drink
		David in Copenhagen - Copenhagen Bar 1 with Absinthe
	e) 1 account has multiple favorite cities
		Eve in Freeport and Freeport (no secret bars)
	f) 1 account has multiple secret bars in 1 city
		frank in Nassau - Nassau Bar 1 and Nassau Bar 3 (no drinks)
	g) 1 account can only have 1 unique account / city / secret bar regardless of drink
	h) 2 accounts with same favorite city but no favorite bars
		Grace AND Heidi in Sydney
	i) 2 accounts with same favorite city but only 1 account has secret bar
		Ivan and Judy in Toronto with Judy secert bar Toronto Bar 1
	j) 2 accounts with same favorite city with 1 similar secret bar
		Mallory and Mike in Perth - Perth Bar 1
	h) 2 accounts with same favorite city with 2 similar secret bars
		Nina and Olvia in Vancouver - both Vancouver Bar 1 and Vancouver bar 2
	